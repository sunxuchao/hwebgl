#HWebgl
========================================


觉得 webgl 原生太复杂 第三方插件也不简单，只想傻瓜的使用独立的3d功能的，来看看 HWebgl吧，使用就是这么简单。

理想抒发完了，接下来一步一步去实现吧。需要的同学尽情提意见哈


#当前状态 第一个功能优化中....   第二功能优化中......

第一个功能    web全景
--------------------------------------------------------------
## 演示地址

使用说明 ：

### 1 先引用插件：
               <script src="../lib/hwebglpano.min.js"></script>

### 2 主体代码：
              var textureArray=['../res/pan01/pano_f.jpg','../res/pan01/pano_b.jpg','../res/pan01/pano_u.jpg','../res/pan01/pano_d.jpg','../res/pan01/pano_r.jpg','../res/pan01/pano_l.jpg']
              //参数: 正面图 id,背面图 id ,左侧id, 右侧id, 顶部id, 底部id
              var hwebgl=new HWebgl('webgl');//参数: canvas id
              hwebgl.CreatePano(textureArray,60.0)
              hwebgl.addEvent('textureComplete',function(){
                     console.log('贴图加载完成.');
                     hwebgl.removeEvent('textureComplete')
              })
              //参数: 贴图数组,视线角度（效果为远近）
               //hwebgl.SetAngle(50.0)//设置远近函数  可用于滚轮 或移动多点 缩放

第二个功能    简单模型加载和展示  (主要用于 单商品的三维展示)
--------------------------------------------------------------
## 演示地址

使用说明 ：

### 1 先引用插件：
            <script src="../lib/hwebglpano.js"></script>

### 2 主体代码：
            var hwebgl=new HWebgl('webgl')
            hwebgl.loadObj('../res/obj/pangea3dgalleon.obj',800.0,1)
           //第一个参数 模型url 第二个参数 视角远近  第三个参数 模型缩放比例


## 运行环境 
   web站点 

第三个功能    三维图片展示
--------------------------------------------------------------
酝酿中........

## 文件说明
   1 lib/hwebglpano.js         全景插件<br>
   2 lib/hwebglobj.js     模型展示插件<br>
   3 lib/*.min.js   压缩文件<br>
   4 src/           开发源码<br>
   5 sample/               一个案例<br>
   6 res                 静态资源<br>

## 版本：
   2017-4-14  :最基础板上线<br>
   2017-4-14 晚: 功能优化 ：支持移动版,提高纹理精度及抗锯齿<br>
   2017-4-15  1 功能优化: 增加设置远近函数  可用于滚轮 或移动多点 缩放  hwebgl.SetAngle(50.0)<br>
              2 代码优化: 修改了代码结构 更易于拓展.<br>
   2017-4-16  增加贴图加载完成事件(案例里有)<br>
   2017-4-17  改变贴图加载方式 减少html代码量，为后续扩展做准备<br>
   2017-4-18  添加gulp 修改整体项目结构 让插件构建自动化<br>
   2017-4-20  添加设置x轴角度函数：hwebgl.SetXAngle(45.0)<br>
   2017-4-21  第二功能开发中...<br>
   2017-4-21 晚  第二功能基础版上线...<br>
   2017-4-24   增加环境光...<br>




